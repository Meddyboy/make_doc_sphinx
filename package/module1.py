#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is the docstring of module1
"""


def add(a, b):
    """Returns the sum of a and b.

    >>> add(1, 2)
    >>> 3

    Args:
        a (int): first operand
        b (int): second operand

    Returns:
        int: sum of a and b
    """

    return a + b


def generator_func(count):
    """Returns a generator of integers from 0 to count.

    Args:
        count (int)

    Yields:
        int

    Raises:
        ValueError: raised when count is not integer.
    """

    if not isinstance(count, int):
        raise ValueError("Expected integer counter got {}."
                         "".format(type(count)))

    for i in xrange(count):
        yield i
