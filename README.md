## Configuration de la Documentation
Ce projet est configuré pour générer de la documentation à l'aide de Sphinx, un générateur de documentation reconnu.

## Informations du Projet
- Nom du Projet: Test Documentation Best Practices
- Auteur: Meddy.B
- Année de Copyright: 2022
- Version: 001
- Configuration de Sphinx
- La configuration de Sphinx comprend des extensions et des options spécifiques pour personnaliser la génération de la documentation.

Parmi les extensions utilisées :

- sphinx.ext.autodoc : pour générer automatiquement la documentation à partir du code source.
- sphinx.ext.napoleon : pour une prise en charge avancée des docstrings.
- sphinx_rtd_theme : pour un thème agréable à la lecture.
- Langue de la Documentation
- La documentation est rédigée en français (language = 'Fr').
