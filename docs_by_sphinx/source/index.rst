.. Test Documentation Best Practices documentation master file, created by
   sphinx-quickstart on Sun Aug 14 17:53:37 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Test Documentation Best Practices's documentation!
=============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

module1
==================

.. automodule:: package.module1
   :members:

module2
==================

.. automodule:: package.module2
   :members:
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
