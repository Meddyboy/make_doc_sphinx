#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
A UserGroup is a group of different user working together.
It is composed of a manager and workers.
User groups are stored onto a Redis server and can be created
using the UserGroup.create class method and retrieved using the
UserGroup.from_name class method.

UserGroup.MissingUserGroupError is raised when trying to get a user
who doesn't exist on the Redis server.
"""

import json
import redis

from error import MissingUserGroupError
from usr import User


class UserGroup:
    """
    Main Class used to handle user groups
    """

    def __init__(self, name, manager=None, workers=None):
        self.__name = name
        self.__mgr = manager
        self.__wks = workers

    @classmethod
    def from_name(cls, user_group_name):
        """
        Returns the unique user group from the given name.

        Args:
            user_group_name (str)

        Returns:
            User
        """
        _redis = redis.Redis()
        redis_data = _redis.get(f"usr_grp_{user_group_name}")

        if not redis_data:
            raise MissingUserGroupError(f"No user group named {user_group_name}")

        dict_data = json.loads(redis_data)

        # Prepare data for initialisation
        workers = [User.from_name(usr_name) for usr_name in dict_data.get("wks", [])]

        if manager_name := dict_data.get("mgr", None):
            mgr = User.from_name(manager_name)
        else:
            mgr = None

        return cls(user_group_name, mgr, workers)

    @classmethod
    def create(cls, name, manager=None, workers=None):
        """Creates a new user group with the given inputs.

        Args:
            name (str)
            manager (User, str or None)
            workers (list[User], list[str] or None)

        Returns:
            UserGroup
        """

        if isinstance(manager, str):
            manager = User.from_name(manager)

        if workers is None:
            workers = []

        workers_clean = []

        for employee in workers:

            _worker = employee

            if isinstance(employee, str):
                _worker = User.from_name(employee)

            workers_clean.append(_worker)

        _data = {}

        if manager is not None:
            _data['mgr'] = manager.name

        if workers:
            _data['wks'] = [w.name for w in workers_clean]

        key = f"usr_grp_{name}"
        redis.Redis().set(key, json.dumps(_data), nx=True)

        return cls(name, manager, workers_clean)

    @property
    def name(self):
        """
        Returns the name of the usergroup

        Args:
            self (UserGroup)

        Returns:
            __name
        """
        return self.__name

    @property
    def manager(self):
        """
        Returns the manager of the usergroup

        Args:
            self (UserGroup)

        Returns:
            __mgr
        """
        return self.__mgr

    @manager.setter
    def manager(self, value):
        assert isinstance(value, (User, type(None))), (type(value), value)

        _data = {'wks': [worker.name for worker in self.__wks]}

        if value is not None:
            _data['manager'] = value.name

        redis.Redis().set(self.__name, json.dumps(_data))

        self.__mgr = value

    @property
    def workers(self):
        """Returns the user group workers.

        Returns:
            list[User]
        """
        return self.__wks

    @workers.setter
    def workers(self, value):
        assert isinstance(value, list), (type(value), value)

        _data = {'mgr': self.__mgr.name}

        if value:
            _data['wks'] = [worker.name for worker in value]

        redis.Redis().set(self.__name, json.dumps(_data))

        self.__wks = value
